class AddColumnsToRestaurantDescriptions < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurant_descriptions, :restaurant_id, :integer
    add_column :restaurant_descriptions, :title, :string
    add_column :restaurant_descriptions, :subtitle, :string
    add_column :restaurant_descriptions, :content, :text
  end
end
