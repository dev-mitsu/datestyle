class AddReputationToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :reputation, :float
  end
end
