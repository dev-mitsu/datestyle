class CreateRestaurantFoodImages < ActiveRecord::Migration[5.2]
  def change
    create_table :restaurant_food_images do |t|
      t.string :image
      t.integer :restaurant_id

      t.timestamps
    end
  end
end
