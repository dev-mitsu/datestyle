class AddColumnsToRestaurantDrinkImages < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurant_drink_images, :price, :integer
    add_column :restaurant_drink_images, :name, :string
  end
end
