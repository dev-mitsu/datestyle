class AddColumnBudgetToRestaurants < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurants, :budget, :integer
  end
end
