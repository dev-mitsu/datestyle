class CreateRestaurantDrinkImages < ActiveRecord::Migration[5.2]
  def change
    create_table :restaurant_drink_images do |t|
      t.string :image
      t.integer :restaurant_id

      t.timestamps
    end
  end
end
