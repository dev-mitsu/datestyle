class AddContentToRestaurantDrinkImages < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurant_drink_images, :content, :text
  end
end
