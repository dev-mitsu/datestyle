class AddColumnsToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :food_reputation, :integer
    add_column :comments, :drink_reputation, :integer
    add_column :comments, :service_reputation, :integer
    add_column :comments, :coupon_reputation, :integer
  end
end
