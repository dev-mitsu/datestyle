class CreateRestaurantDescriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :restaurant_descriptions do |t|

      t.timestamps
    end
  end
end
