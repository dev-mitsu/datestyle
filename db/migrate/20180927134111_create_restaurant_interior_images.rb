class CreateRestaurantInteriorImages < ActiveRecord::Migration[5.2]
  def change
    create_table :restaurant_interior_images do |t|
      t.string :image
      t.string :name

      t.timestamps
    end
  end
end
