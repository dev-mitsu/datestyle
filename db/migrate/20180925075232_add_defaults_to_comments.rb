class AddDefaultsToComments < ActiveRecord::Migration[5.2]
  def change
    change_column :comments, :food_reputation, :integer, default: 0
    change_column :comments, :drink_reputation, :integer, default: 0
    change_column :comments, :service_reputation, :integer, default: 0
    change_column :comments, :coupon_reputation, :integer, default: 0
  end
end
