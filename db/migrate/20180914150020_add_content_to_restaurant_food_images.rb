class AddContentToRestaurantFoodImages < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurant_food_images, :content, :text
  end
end
