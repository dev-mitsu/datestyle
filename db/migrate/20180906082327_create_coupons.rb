class CreateCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :coupons do |t|
      t.string :name
      t.integer :restaurant_id
      t.text :content
      t.timestamps
    end
  end
end
