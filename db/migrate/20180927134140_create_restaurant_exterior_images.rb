class CreateRestaurantExteriorImages < ActiveRecord::Migration[5.2]
  def change
    create_table :restaurant_exterior_images do |t|
      t.string :image
      t.string :name

      t.timestamps
    end
  end
end
