class AddPriorityToRestaurants < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurants, :priority, :boolean, default: false
  end
end
