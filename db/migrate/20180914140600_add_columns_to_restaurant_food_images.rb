class AddColumnsToRestaurantFoodImages < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurant_food_images, :price, :integer
    add_column :restaurant_food_images, :name, :string
  end
end
