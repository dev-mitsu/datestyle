class AddCategoriesToRestaurants < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurants, :category_id, :integer
    add_column :restaurants, :sub_category_id, :integer
  end
end
