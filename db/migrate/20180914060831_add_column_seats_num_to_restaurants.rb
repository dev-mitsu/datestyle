class AddColumnSeatsNumToRestaurants < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurants, :seat_num, :integer
  end
end
