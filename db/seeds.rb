# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
AdminUser.create(email: "existplus.ito@gmail.com", password: "1existplus1", password_confirmation: "1existplus1")

100.times do |i|
  r = Restaurant.new(description: "テスト"*50, restaurant_name: "BAR SAMPLE", address: "東京都豊島区池袋4-5-6 丸沢ビル5F", phone: "0332144214", email: "aaaaa#{i+1}@gmail.com", password: "aaaaaa")
  r.save(validate: false)
end

# 100.times do |i|
#   Restaurant.find(i+1).coupons.create(name:"0円飲み放題", content: "0円で飲み放題が利用できます。")
# end
#
# a = User.new(name: "たけみつ", email: "developer.mitsu@gmail.com", password: "xmy575974")
# a.save(validate: false)
#
100.times do |i|
  u = User.new(name: "サンプル", email: "sample#{i}@sample.com", password: "sample")
  u.save(validate: false)
end

100.times do |i|
  f= rand(5)+1
  d= rand(5)+1
  c= rand(5)+1
  s= rand(5)+1
  ave = (f+d+c+s)/4

  Comment.create(comment:"test"*rand(50),
                                 user_id: rand(100)+1,
                                 restaurant_id: rand(100)+1,
                                 food_reputation: f,
                                 drink_reputation: d,
                                 coupon_reputation: c,
                                 service_reputation: s,
                                 reputation: ave.round(2)
  )

#
# 5.times do |i|
#  Category.create(name: "testParent#{i+1}")
# end
#
# 5.times do |i|
#   5.times do |ii|
#     Category.find(i+1).sub_categories.create(name: "testChild#{ii+1}")
#   end
# end

end