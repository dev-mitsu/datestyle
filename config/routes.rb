Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # devise_for :restaurants
  get '/search' , to: 'pages#index'
  root 'pages#top'
  get '/how_to', to: 'pages#how_to'

  post 'restaurants/:id/description', to: 'restaurant_descriptions#create'
  patch 'restaurants/:id/description', to: 'restaurant_descriptions#update'
  put   'restaurants/:id/description', to: 'restaurant_descriptions#update'

  post 'restaurant/image/food', to: 'restaurant_images#set_food_images'
  delete 'restaurant/image/food/:id', to: 'restaurant_images#destroy_food_images'

  post 'restaurant/image/drink', to: 'restaurant_images#set_drink_images'
  delete 'restaurant/image/drink/:id', to: 'restaurant_images#destroy_food_images'

  post 'restaurant/image/facility', to: 'restaurant_images#set_facility_images'
  delete 'restaurant/image/facility/:id', to: 'restaurant_images#destroy_facility_images'




  devise_for :users, controllers: {
      sessions: 'users/sessions',
      passwords: 'users/passwords',
      registrations: 'users/registrations'
}

  devise_scope :user do
    get 'users/complete', to: 'users/registrations#complete'

    post 'users/sign_up/confirm' => 'users/registrations#confirm'
    post 'users/sign_up/complete' => 'users/registrations#complete'
    get 'users/:id' => 'users/registrations#show', as: 'user_show'

  end

  devise_for :restaurants, controllers: {
      sessions: 'restaurants/sessions',
      passwords: 'restaurants/passwords',
      registrations: 'restaurants/registrations'
  }

  get 'restaurants/:id', to: 'pages#show', as: 'restaurant_show'

  resources :coupons , only: [:create, :destroy]
  resources :comments , only: [:create, :destroy]

  get '/coupon/use/:restaurant_id/:coupon_id', to: 'coupons#use_coupon'



  # devise_for :users, :controllers => {
  #     :registrations => 'users/registrations'
  # }


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
