class Restaurant < ApplicationRecord
  # Include default users modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  acts_as_taggable_on :keywords
  has_one :restaurant_description, dependent: :destroy
  has_many :restaurant_drink_images, dependent: :destroy
  has_many :restaurant_food_images, dependent: :destroy
  has_many :restaurant_facility_images, dependent: :destroy

  has_many :coupons, dependent: :destroy
  has_many :comments, dependent: :destroy

  belongs_to :category
  belongs_to :sub_category

  geocoded_by :address
  after_validation :geocode

  mount_uploader :image, ImageUploader

  def update_without_current_password(params, *options)
    params.delete(:current_password)

    if params[:password].blank? && params[:password_confirmation].blank?
      params.delete(:password)
      params.delete(:password_confirmation)
    end

    result = update_attributes(params, *options)
    clean_up_passwords
    result
  end

  def self.search_by_name(search)
    if search
      Restaurant.where(['restaurant_name LIKE ?', "%#{search}%"])
    else
      Restaurant.all #全て表示。
    end
  end
end
