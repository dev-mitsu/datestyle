class Coupon < ApplicationRecord
  has_many :used_coupons
  belongs_to :restaurant
end
