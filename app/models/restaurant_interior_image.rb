class RestaurantInteriorImage < ApplicationRecord
  mount_uploader :image, RestaurantInteriorImageUploader
end
