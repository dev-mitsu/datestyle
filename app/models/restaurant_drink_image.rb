class RestaurantDrinkImage < ApplicationRecord
  belongs_to :restaurant
  mount_uploader :image, RestaurantDrinkImageUploader
end
