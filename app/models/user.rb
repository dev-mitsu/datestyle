class User < ApplicationRecord
  # Include default users modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validate :accepted_valid?

  has_many :used_coupons
  has_many :comments




  private

    def accepted_valid?
      errors.add(:accepted, 'して頂かない場合は、登録できません。') unless accepted == true
    end
end
