class RestaurantFoodImage < ApplicationRecord
  belongs_to :restaurant
  mount_uploader :image, RestaurantFoodImageUploader
end
