module ApplicationHelper

  def average_reputation(restaurant)
    unless restaurant.comments.blank?
      restaurant.comments.average(:reputation)
    else
      return 100
    end
  end


end
