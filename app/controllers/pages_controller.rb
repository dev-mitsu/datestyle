class PagesController < ApplicationController
  def top
    @restaurants = Restaurant.page(params[:page]).per(5).order('created_at DESC')
    @tags = ActsAsTaggableOn::Tag.most_used(3)
    @tags = Restaurant.all_tags

    @prior_restaurants = Restaurant.where(priority: true)


  end

  def show
    @restaurant = Restaurant.find(params[:id])
    if @restaurant.restaurant_description
      @restaurant_description = @restaurant.restaurant_description
    else
      @restaurant_description = RestaurantDescription.new

    end
    @drink_images = @restaurant.restaurant_drink_images.all
    @food_images = @restaurant.restaurant_food_images.all

    @coupons = @restaurant.coupons.all
    @comment = User.first.comments.build
    @comments = @restaurant.comments.all
  end

  def index
    if params[:category].present? && params[:name].present?

      restaurants = Restaurant.tagged_with(params[:category])

      @restaurants = restaurants.select{|k,v| k==:restaurant_name && v==params[:name]}

    elsif params[:category] && params[:name].blank?

      @restaurants = Restaurant.tagged_with(params[:category])

    elsif params[:category].blank? && params[:name]

      @restaurants = Restaurant.search_by_name(params[:name])

    end

    @searched_name = params[:name]
    @searched_category = params[:category]

  end

end
