class CommentsController < ApplicationController

  def create
    if current_user

      comment = Restaurant.find(params[:restaurant_id]).comments.build(comment_params)
      comment.user_id = current_user.id
      comment.reputation = average_reputation_by_comment(comment)

      comment.save!

      redirect_to "/restaurants/#{params[:restaurant_id]}"

    else

      flash[:alert] = "レビュー投稿には会員登録が必要です。"

      redirect_to "/users/sign_up"

    end

  end

  def destroy
    Comment.find(params[:id]).destroy
  end

  private
    def comment_params
      params.require(:comment).permit(:comment, :restaurant_id, :food_reputation, :drink_reputation,
                                      :service_reputation, :coupon_reputation)
    end

    def average_reputation_by_comment(comment)
      all_reputation = comment.food_reputation+comment.drink_reputation+comment.coupon_reputation+comment.service_reputation

      (all_reputation/4).round(2)
    end


end
