class RestaurantDescriptionsController < ApplicationController

  def create
    restaurant = Restaurant.find(params[:id])
    rd = RestaurantDescription.new(restaurant_description_params)
    restaurant.restaurant_description = rd

    if rd.save
      redirect_to edit_restaurant_registration_path
    end

  end

  def update
    restaurant = Restaurant.find(params[:id])
    rd = RestaurantDescription.update(restaurant_description_params)
    restaurant.restaurant_description = rd

    redirect_to edit_restaurant_registration_path

  end

  private

    def restaurant_description_params
      params.require(:restaurant_description).permit(:content, :title, :subtitle)
    end
end
