# frozen_string_literal: true

class Restaurants::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end
  #


  # POST /resource
  def create
    super
    current_restaurant.coupons.create(name: "0円飲み放題クーポン", content: "無料で飲み放題が利用できます。")
  end

  # GET /resource/edit
  def edit
    # if current_restaurant.restaurant_description
    #   @restaurant_description = current_restaurant.restaurant_description
    # else
    #   @restaurant_description = RestaurantDescription.new
    #
    # end

    @restaurant_food_images = current_restaurant.restaurant_food_images.all
    @restaurant_drink_images = current_restaurant.restaurant_drink_images.all

    @coupons =current_restaurant.coupons.all
    @coupon = current_restaurant.coupons.build

    super


  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end


  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    # super(resource)
    edit_restaurant_registration_path
  end

  def after_update_path_for(resource)
    # super(resource)
    edit_restaurant_registration_path
  end


  protected

  def update_resource(resource, params)
    resource.update_without_current_password(params)
  end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
