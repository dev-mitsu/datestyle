class CouponsController < ApplicationController
  def create
    current_restaurant.coupons.create(coupon_params)

    redirect_to "/restaurants/edit"
  end

  def destroy
    Coupon.find(params[:id]).destroy
    redirect_to "/restaurants/edit"
  end

  def use_coupon
    if current_user
      UsedCoupon.create(user_id: current_user.id, coupon_id: params[:coupon_id])
      redirect_to restaurant_show_url(params[:restaurant_id])
    else
      flash[:alert] = "クーポンのご利用にはログインが必要です。"
      redirect_to "/users/sign_in"
    end
  end

  private
    def coupon_params
      params.require(:coupon).permit(:name, :content)
    end
end
