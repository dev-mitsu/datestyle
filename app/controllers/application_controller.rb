class ApplicationController < ActionController::Base
  # before_action :configure_permitted_parameters, if: :devise_controller?

  # protected
  #
  # def configure_permitted_parameters
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :accepted])
  #   devise_parameter_sanitizer.permit(:confirm, keys: [:name, :email, :accepted])
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:name, :email])
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:name, :email])
  # end

  protected

  def devise_parameter_sanitizer
    if resource_class == User
      UserParameterSanitizer.new(User, :user, params)
    elsif resource_class == Restaurant
      RestaurantParameterSanitizer.new(Restaurant, :restaurant, params)
    else
      super # Use the default one
    end
  end
end


class RestaurantParameterSanitizer < Devise::ParameterSanitizer
  def initialize(*)
    super
    permit(:sign_up, keys: [:restaurant_name, :email, :address, :postal, :phone, :latitude, :longitude])
    permit(:sign_confirm, keys: [:restaurant_name, :email, :address, :postal, :phone, :latitude, :longitude])
    permit(:sign_in, keys: [:restaurant_name, :email, :keyword_list])
    permit(:account_update, keys: [:restaurant_name, :email, :address, :postal, :phone, :keyword_list, :seat_num, :budget, :latitude, :longitude, :description, :image, :sub_category_id, :category_id])
  end
end

class UserParameterSanitizer < Devise::ParameterSanitizer
  def initialize(*)
    super
    permit(:sign_up, keys: [:name, :email, :accepted])
    permit(:sign_confirm, keys: [:name, :email, :accepted])
    permit(:sign_in, keys: [:name, :email])
    permit(:account_update, keys: [:name, :email])
  end
end