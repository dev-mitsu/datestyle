class RestaurantImagesController < ApplicationController

  def set_food_images
    restaurant = Restaurant.find(current_restaurant.id)
    restaurant.restaurant_food_images.create(restaurant_food_image_params)
    redirect_to "/restaurants/edit"
  end

  def set_drink_images
    restaurant = Restaurant.find(current_restaurant.id)
    restaurant.restaurant_drink_images.create(restaurant_drink_image_params)
    redirect_to "/restaurants/edit"
  end


  def destroy_food_images
    @image = RestaurantFoodImage.find(params[:id])
    @image.destroy
    redirect_to "/restaurants/edit"
  end

  def destroy_drink_images
    @image = RestaurantDrinkImage.find(params[:id])
    @image.destroy
    redirect_to "/restaurants/edit"
  end



  private

    def restaurant_food_image_params
      params.require(:restaurant_food_image).permit(:content, :image, :name, :price)
    end


    def restaurant_drink_image_params
      params.require(:restaurant_drink_image).permit(:content, :image, :name, :price)
    end
end
