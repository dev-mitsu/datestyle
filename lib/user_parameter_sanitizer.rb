class UserParameterSanitizer < Devise::ParameterSanitizer
  def initialize(*)
    super
    permit(:sign_up, keys: [:name, :email, :accepted])
    permit(:sign_confirm, keys: [:name, :email, :accepted])
    permit(:sign_in, keys: [:name, :email])
    permit(:account_update, keys: [:name, :email])
  end
end