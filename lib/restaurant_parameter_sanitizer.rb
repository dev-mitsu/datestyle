class RestaurantParameterSanitizer < Devise::ParameterSanitizer
  def initialize(*)
    super
    permit(:sign_up, keys: [:restaurant_name, :email, :address, :postal, :phone, :latitude, :longitude])
    permit(:sign_confirm, keys: [:restaurant_name, :email, :address, :postal, :phone, :latitude, :longitude])
    permit(:sign_in, keys: [:restaurant_name, :email, :keyword_list])
    permit(:account_update, keys: [:restaurant_name, :email, :address, :postal, :phone, :keyword_list, :seat_num, :budget, :latitude, :longitude, :description, :image])
  end
end